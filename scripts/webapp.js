const express= require('express');
var Pool= require('pg').Pool;
var bodyParser= require('body-parser');

const app= express();
var config= {
	host: 'localhost',
	user: 'manager',
	password: 'g~N]mD:>(9y2A,5S',
	database: 'food_nutrition',
};

var pool= new Pool(config);

app.set('port', (process.env.PORT || 8081));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
	res.setHeader('Access-Controll-Allow-Origin', '*');
	next();
});

if (process.env.NODE_ENV === 'production'){
	console.log('running production server');
}
app.use(express.static('../build'));

app.get('/food', async (req, res) => {
	var entry= req.query.entry;
	try{
		var response= await pool.query('select description, kcal, protein_g, cast(fa_sat_g + fa_mono_g + fa_poly_g as decimal(5,2)) as "fat_g", carbohydrate_g from entries where description like $1 limit 25;',['%'+entry+'%']);
		if (entry == undefined){
			res.json({error: 'paramaters not given'})
		}else{
			res.json({entries: response.rows.map(function (item){
				return {description: item.description, kcal: item.kcal, protein_g: item.protein_g, fat_g: item.fat_g, carbohydrate_g: item.carbohydrate_g};
			})});
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.listen(app.get('port'), () => {
	console.log('Running');
})


