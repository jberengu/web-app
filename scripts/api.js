var axios = require('axios');

module.exports= {
	fetchData: function (search){
		var encodedURI = ('http://localhost:8081/food?entry='+search);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return response.data;
		});
	}
}