import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const api = require('../scripts/api');

class App extends Component {
    state = { loading: false };
    constructor(props) {
        super(props);
        this.state = { search: '' };
        this.state = { data: { entries: [] } };

        this.updateSearch = this.updateSearch.bind(this);
    }

    updateSearch(event) {
        this.setState({ search: event.target.value.substr(0, 20) });
        api.fetchData(event.target.value).then((results) => {
            if (results !== null) {
                return this.setState(() => ({
                    data: results,
                }));
            }
            return this.setState(() => ({
                data: { entries: [] },
            }));
        });
    }

    componentDidMount() {
    }

    render() {
        const data = this.state.data.entries;
        console.log(data);
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Nutritional Information About Food</h2>
                </div>
                <p className="App-intro">
                    Food Search
                </p>
                <div className="search-box">
                    <form>
                        <input
                            className="form-control"
                            type="text"
                            value={this.state.search}
                            onChange={this.updateSearch}
                            name="food-input"
                            placeholder="Search foods..." />
                    </form>
                </div>
                {this.state.search !== '' ? (
                    <div>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Kcal</th>
                                    <th>Protein(g)</th>
                                    <th>Fat(g)</th>
                                    <th>Carbs(g)</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.map(info => (<tr>
                                    <td>{info.description}</td>
                                    <td>{info.kcal}</td>
                                    <td>{info.protein_g}</td>
                                    <td>{info.fat_g}</td>
                                    <td>{info.carbohydrate_g}</td>
                                </tr>))}
                            </tbody>
                        </table>
                    </div>) : (<div />)}
            </div>


        );
    }
}

export default App;
